#!/usr/bin/python3

#finish type = exit

import socket, sys
ip = input('Target IP: ')
port = 0
try:
    port = int(input('Port: '))
except ValueError:
    print(f'Error with port: {port}')

try:
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((ip, port))
    sms = ''

    while True:
        sms = input('Type a sms client: ').encode('utf-8')
        client_socket.send(sms)

        if sms == b'exit':
            sys.exit(0)

        sms = client_socket.recv(1024).decode('utf-8')
        print(sms)

    client_socket.close()
    print('[*] Exittttttt')

except socket.error:
    print('[*] Fail to connect :(')