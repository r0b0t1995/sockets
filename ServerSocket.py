#!/usr/bin/python3
import socket, sys
ip = input('My IP: ')
port = 0
try:
    port = int(input('Port: '))
except ValueError:
    print(f'Error with port: {port}')

try:
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((ip, port))
    server_socket.listen()
    print('[*] Listen')
    client, addr = server_socket.accept()
    print(f'[*] Connect with {addr}')
    sms = ''
    while True:

        sms = client.recv(1024).decode('utf-8')
        print(f'Client response = {sms}')

        if sms == 'exit':
            sys.exit(0)

        #sms = input('Type a sms server: ').encode('utf-8')
        sms = b'server response :)'
        client.send(sms)

    server_socket.close()
    print('[*] Exit server')

except socket.error:
    print('[*] Fail to connect :(')
